#include "editorwindow.h"
#include "ui_editorwindow.h"

#include "groove_spiral.h"

#include "../oscilloscope/oscilloscope.h"

#include "../hardware/dac_perfect.h"
#include "../hardware/equalizer_perfect.h"
#include "../hardware/recorder_perfect.h"
#include "../simulation/library.h"

#include <QDebug>
#include <QTimer>

EditorWindow::EditorWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::EditorWindow)
{
	ui->setupUi(this);

	ui->mainToolBar->setHidden(true);
	ui->statusBar->setHidden(true);
	ui->menuBar->setHidden(true);

	library_t * library = new library_t;

	dac_perfect_t * dac = new dac_perfect_t;
	dac->set_library(library);

	equalizer_perfect_t * eq = new equalizer_perfect_t;
	eq->set_source(dac);
	eq->set_mode(equalizer_perfect_t::MODE_RIAA_RECORDING);

	recorder_perfect_t * rec = new recorder_perfect_t;
	rec->set_source(dac);
	rec->get_spiral().data.emplace_back(0.0, rec->get_spiral().get_radius_max() - 0.002 * 1000000.0);
	rec->get_spiral().data.emplace_back(5.0, rec->get_spiral().get_radius_max() - 0.005 * 1000000.0);
	//rec->get_spiral().data.emplace_back(30.0, 0.12 * 1000000.0);
	rec->get_spiral().data.emplace_back(50.0, 0.09 * 1000000.0);
	rec->get_spiral().data.emplace_back(55.0, 0.04 * 1000000.0);
	rec->get_spiral().data.emplace_back(60.0, 0.04 * 1000000.0);

	ui->widget->set_recorder(rec);

	/*
	oscilloscope_t * oscilloscope = new oscilloscope_t();

	oscilloscope->analyze_freq_response(eq);

	std::vector<std::pair<double, double>> vec[4];

	double tstart = 0.5;
	double tmax = 0.01;
	double tstep = 0.00001;

	for(double t = tstart; t < tstart + tmax; t += tstep)
	{
		auto v1 = dac->sample(t);
		auto v2 = eq->sample(t);
		vec[0].emplace_back(t, v1.l);
		vec[1].emplace_back(t, v1.r);
		vec[2].emplace_back(t, v2.l);
		vec[3].emplace_back(t, v2.r);
	}

	//oscilloscope->add(vec[0], 0);
	//oscilloscope->add(vec[1], 1);
	//oscilloscope->add(vec[2], 2);
	//oscilloscope->add(vec[3], 3);

	oscilloscope->show();
	*/

	//QTimer::singleShot(10, this, SLOT(close()));
}

EditorWindow::~EditorWindow()
{
	delete ui;
}
