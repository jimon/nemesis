#pragma once

#include <QMainWindow>

namespace Ui {
class EditorWindow;
}

class EditorWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit EditorWindow(QWidget *parent = 0);
	~EditorWindow();

private:
	Ui::EditorWindow *ui;
};
