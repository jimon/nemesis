#include "oscilloscope.h"
#include "ui_oscilloscope.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include <limits>

oscilloscope_t::oscilloscope_t(QWidget * parent)
	:QWidget(parent),
	ui(new Ui::oscilloscope_t)
{
	ui->setupUi(this);
}

oscilloscope_t::~oscilloscope_t()
{
	delete ui;
}

void oscilloscope_t::add(const std::vector<std::pair<double, double>> & add_data, size_t channel)
{
	for(const auto &p: add_data)
		channels[channel].push_back(p);
}

void oscilloscope_t::clear()
{
	channels.clear();
}

void oscilloscope_t::refresh()
{
	if(!channels.size() || !channels.begin()->second.size())
	{
		ui->graphicsView->setScene(nullptr);
		return;
	}

	QGraphicsScene * scene = new QGraphicsScene(this);

	const std::vector<QRgb> colors = {
		0x002b36, 0x073642, 0x586e75, 0x657b83, 0x839496, 0x93a1a1, 0xeee8d5, 0xfdf6e3,
		0xb58900, 0xcb4b16, 0xdc322f, 0xd33682, 0x6c71c4, 0x268bd2, 0x2aa198, 0x859900};

	scene->setBackgroundBrush(QBrush(colors[0]));

	QString text;
	std::pair<double, double> h(channels.begin()->second.begin()->first, channels.begin()->second.begin()->first);
	std::pair<double, double> v(channels.begin()->second.begin()->second, channels.begin()->second.begin()->second);

	for(auto it = channels.begin(); it != channels.end(); ++it)
	{
		const auto & data = it->second;

		std::pair<double, double> ht(data.begin()->first, data.begin()->first);
		std::pair<double, double> vt(data.begin()->second, data.begin()->second);

		for(const std::pair<double, double> & p: data)
		{
			ht.first =  std::min(p.first, ht.first);
			ht.second = std::max(p.first, ht.second);
			vt.first =  std::min(p.second, vt.first);
			vt.second = std::max(p.second, vt.second);
		}

		QString log = QString("ch %1\nV : [%2, %3]\nH : [%4, %5]\n")
						.arg(it->first)
						.arg(vt.first, 8).arg(vt.second, 8)
						.arg(ht.first, 8).arg(ht.second, 8);
		text += log;

		h.first =  std::min(ht.first,  h.first);
		h.second = std::max(ht.second, h.second);
		v.first =  std::min(vt.first,  v.first);
		v.second = std::max(vt.second, v.second);
	}

	double hs = h.second - h.first;
	double vs = v.second - v.first;

	h.first  -= hs / (zoom * 2.0);
	h.second += hs / (zoom * 2.0);
	v.first  -= vs / (zoom * 2.0);
	v.second += vs / (zoom * 2.0);

	hs /= zoom;
	vs /= zoom;

	QSize size = ui->graphicsView->size();
	QPointF center((double)size.width() / 2.0, (double)size.height() / 2.0);
	double min_size = (double)std::min(size.width(), size.height());

	for(auto it = channels.begin(); it != channels.end(); ++it)
	{
		const auto & data = it->second;
		auto conv = [center, min_size, h, v, hs, vs](std::pair<double, double> p)->QPointF
		{
			double x = p.first;
			double y = p.second;

			double xp = (x - h.first) / hs;
			double yp = (y - v.first) / vs;

			QPointF out;
			out.setX(xp * min_size - min_size / 2.0);
			out.setY(yp * min_size - min_size / 2.0);

			return out;
		};

		QPointF last = conv(*data.begin());
		QPen pen(colors[it->first + 8]);
		for(const std::pair<double, double> & p: data)
		{
			QPointF next = conv(p);
			scene->addLine(QLineF(last, next), pen);
			last = next;
		}
	}

	if(ui->graphicsView->scene())
		ui->graphicsView->scene()->deleteLater();
	ui->graphicsView->setScene(scene);

	ui->plainTextEdit->document()->setPlainText(text);
}

class freq_generator_t : public signal_source_t
{
public:
	double freq = 10.0;

	signal_sample_t sample(double t)
	{
		signal_sample_t out;
		out.l = sin(2 * M_PI * freq * t);
		out.r = sin(2 * M_PI * freq * t);
		return line_out.to_line(out);
	}
};

void oscilloscope_t::analyze_freq_response(signal_filter_t * filter)
{
	clear();

	signal_source_t * old = filter->get_source();

	freq_generator_t generator;
	filter->set_source(&generator);

	std::vector<std::pair<double, double>> out;

	auto measure_power = [&generator, &filter](double freq)->double
	{
		generator.freq = freq;

		double sum = 0.0;
		size_t steps = 0;

		for(double t = 1.0; t < 1.0 + 5.0 / freq; t += 0.05 / freq)
		{
			double v = filter->sample(t).l;
			sum += v * v;
			steps++;
		}

		sum /= (double)steps;
		return sum;
	};

	double power_1khz = measure_power(1000.0);

	for(double freq = 20.0; freq < 20000.0; freq += std::min(100.0, freq / 20.0))
	{
		double power = measure_power(freq);

		double out_f = log10(freq);
		double out_p = 10.0 * log10(power / power_1khz);
		//double out_f = freq;
		//double out_p = power / power_1khz;

		out.emplace_back(out_f, out_p);
	}

	add(out);

	filter->set_source(old);
}

void oscilloscope_t::resizeEvent(QResizeEvent * event)
{
	refresh();
	QWidget::resizeEvent(event);
}

void oscilloscope_t::on_zoomSpinBox_valueChanged(double arg1)
{
	double set_zoom = std::max(std::min(zoom_max, arg1), zoom_min);
	ui->zoomSpinBox->setValue(set_zoom);

	if(fabs(set_zoom - zoom) >= std::numeric_limits<double>::epsilon())
	{
		zoom = set_zoom;
		refresh();
	}
}

