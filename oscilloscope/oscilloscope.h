#pragma once

#include <QWidget>
#include <vector>
#include <stdint.h>
#include "signal_source.h"

namespace Ui
{
	class oscilloscope_t;
}

class oscilloscope_t : public QWidget
{
	Q_OBJECT

public:
	explicit oscilloscope_t(QWidget * parent = nullptr);
	~oscilloscope_t();

	void add(const std::vector<std::pair<double, double>> & add_data, size_t channel = 0);
	void clear();
	void refresh();

	void analyze_freq_response(signal_filter_t * filter);

protected:
	void resizeEvent(QResizeEvent * event);

private slots:
	void on_zoomSpinBox_valueChanged(double arg1);

private:
	Ui::oscilloscope_t * ui;
	std::map<size_t, std::vector<std::pair<double, double>>> channels;
	double zoom = 0.9;

	const double zoom_min = 0.1;
	const double zoom_max = 20.0;
};

