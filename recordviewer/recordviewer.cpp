#include "recordviewer.h"
#include "ui_recordviewer.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include <QImage>
#include <QImageWriter>
#include <QMouseEvent>
#include <QTimer>
#include <QPainter>
#include <QDebug>

RecordViewerLabel::RecordViewerLabel(QWidget * parent)
	:QLabel(parent)
{
}

void RecordViewerLabel::resizeEvent(QResizeEvent * ev)
{
	QLabel::resizeEvent(ev);
	emit onResize();
}


RecordViewer::RecordViewer(QWidget * parent)
	:QWidget(parent),
	ui(new Ui::RecordViewer)
{
	ui->setupUi(this);
	connect(ui->preview, SIGNAL(onResize()), this, SLOT(refresh()));
}

RecordViewer::~RecordViewer()
{
	delete ui;
}

void RecordViewer::set_recorder(recorder_t * set)
{
	recorder = set;

	if(!recorder)
		return;

	refresh();
	return;

	/*

	const std::vector<QRgb> colors = {
		0x002b36, 0x073642, 0x586e75, 0x657b83, 0x839496, 0x93a1a1, 0xeee8d5, 0xfdf6e3,
		0xb58900, 0xcb4b16, 0xdc322f, 0xd33682, 0x6c71c4, 0x268bd2, 0x2aa198, 0x859900};

	*/
}

void RecordViewer::refresh()
{
	if(refresh_timer)
		return;
	else
	{
		refresh_timer = new QTimer(this);
		refresh_timer->setSingleShot(true);
		connect(refresh_timer, SIGNAL(timeout()), this, SLOT(redraw()));
		refresh_timer->start(10);
	}
}

void RecordViewer::redraw()
{
	if(refresh_timer)
	{
		refresh_timer->deleteLater();
		refresh_timer = nullptr;
	}

	const groove_spiral_t & spiral = recorder->get_spiral();

	// -----------------------------------------------------------

	// figure out preview rectangle

	QSizeF imgSize = ui->preview->size();

	double zoom = ui->zoomSpinBox->value();
	double a_rot = -ui->rotationSpinBox->value() * M_PI / 180.0;
	double max_img_r = std::min(imgSize.width(), imgSize.height()) * zoom / 2.0;

	QPointF boxPos = sliders_pos();
	boxPos.setY(-boxPos.y());

	QPointF boxSize = QPointF(imgSize.width() / max_img_r, imgSize.height() / max_img_r);
	QRectF box(boxPos - boxSize / 2.0, boxPos + boxSize / 2.0);

	//qDebug() << box;

	// -----------------------------------------------------------

	// figure out on what segment of record we are looking now

	/*double a_min = 0.0;
	double a_max = 2.0 * M_PI;

	if(!box.contains(0.0, 0.0))
	{
		auto f = [](QPointF pos)->double
		{
			double a = atan2(pos.y(), pos.x());
			if(a < 0.0)
				a = 2.0 * M_PI + a;
			return a;
		};

		double a[8];
		a[0] = f(box.topLeft());
		a[1] = f(box.topRight());
		a[2] = f(box.bottomLeft());
		a[3] = f(box.bottomRight());
		a[4] = f((box.topLeft() + box.topRight()) / 2.0);
		a[5] = f((box.topLeft() + box.bottomLeft()) / 2.0);
		a[6] = f((box.topRight() + box.bottomRight()) / 2.0);
		a[7] = f((box.bottomLeft() + box.bottomRight()) / 2.0);

		a_min = std::min({a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]});
		a_max = std::max({a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]});
	}*/

	std::vector<std::pair<double, double>> time_segments;

	bool time_segment_was_there = false;
	double time_segment_last = 0.0;
	const double time_segment_delta = 0.001;
	for(double t = 0.0; t <= spiral.playtime; t += time_segment_delta)
	{
		double a = spiral.get_angular_position(t);
		double r1 = (spiral.get_radius(t) - 10) / spiral.get_radius_max();
		double r2 = (spiral.get_radius(t) + 10) / spiral.get_radius_max();

		QPointF p(cos(a + a_rot), sin(a + a_rot));

		QPointF p1 = p * r1;
		QPointF p2 = p * r2;

		QPointF pmin(std::min(p1.x(), p2.x()), std::min(p1.y(), p2.y()));
		QPointF pmax(std::max(p1.x(), p2.x()), std::max(p1.y(), p2.y()));

		QRectF plook(pmin, pmax);

		if (box.contains(plook))
		{
			if(!time_segment_was_there)
			{
				time_segment_last = t - time_segment_delta;
				time_segment_was_there = true;
			}
		}
		else
		{
			if(time_segment_was_there)
			{
				time_segments.emplace_back(time_segment_last, t + time_segment_delta);
				time_segment_was_there = false;
			}
		}
	}

	if(time_segment_was_there)
	{
		time_segments.emplace_back(time_segment_last, spiral.playtime + time_segment_delta);
		time_segment_was_there = false;
	}

	//qDebug() << "----";
	//for(auto p:time_segments)
	//	qDebug() << p.first << p.second;

	// -----------------------------------------------------------

	//qDebug() << a_min << a_max;

	QImage out(imgSize.toSize(), QImage::Format_ARGB32);
	out.fill(qRgb(64, 64, 64));

	QPainter painter(&out);

	auto get_pixel = [&box, &boxPos, &imgSize, &max_img_r](double x, double y)->QPointF
	{
		x -= boxPos.x();
		y -= boxPos.y();

		x *= max_img_r;
		y *= max_img_r;

		x = x + imgSize.width() / 2.0;
		y = imgSize.height() / 2.0 - y;

		return {x, y};
	};

	auto draw_pixel = [&get_pixel, &box, &imgSize, &out](double x, double y, QColor color)
	{
		if (!box.contains(x, y))
			return;

		QPointF point = get_pixel(x, y);

		if (point.x() >= 0.0 && point.x() < imgSize.width() && point.y() >= 0.0 && point.y() < imgSize.height())
		{
			QRgb * pixel = (QRgb*)(out.scanLine((int)point.y()) + (int)point.x() * sizeof(QRgb));
			*pixel = color.rgb();
		}
	};

	auto draw_line = [&get_pixel, &painter](double x1, double y1, double x2, double y2, QColor color)
	{
		QPointF point1 = get_pixel(x1, y1);
		QPointF point2 = get_pixel(x2, y2);
		painter.setPen(color);
		painter.drawLine(point1, point2);
	};

	auto draw_circle = [&draw_pixel, &max_img_r](double x, double y, double r, QColor color)
	{
		for(double a = 0.0; a <= 2.0 * M_PI; a += 1.0 / (2.0 * M_PI * r * max_img_r))
			draw_pixel(cos(a) * r + x, sin(a) * r + y, color);
	};

	auto draw_sample = [&a_rot, &draw_line, &spiral, this](double t, QColor color1, QColor color2)
	{
		groove_sample_t s = recorder->sample(t);

		double x = cos(s.a + a_rot);
		double y = sin(s.a + a_rot);

		double xc = x * s.r / spiral.get_radius_max();
		double yc = y * s.r / spiral.get_radius_max();

		double x1 = x * (s.r - s.w / 2.0) / spiral.get_radius_max();
		double y1 = y * (s.r - s.w / 2.0) / spiral.get_radius_max();

		double x2 = x * (s.r + s.w / 2.0) / spiral.get_radius_max();
		double y2 = y * (s.r + s.w / 2.0) / spiral.get_radius_max();

		draw_line(x1, y1, xc, yc, color1);
		draw_line(x2, y2, xc, yc, color2);
	};

	/*draw_circle(  0,  0, 0.01, Qt::red);
	draw_circle(  1,  0, 0.01, Qt::red);
	draw_circle( -1,  0, 0.01, Qt::cyan);
	draw_circle(  0,  1, 0.01, Qt::yellow);
	draw_circle(  0, -1, 0.01, Qt::blue);*/

	draw_line(0, 0, cos(a_rot + M_PI * 0.0), sin(a_rot + M_PI * 0.0), Qt::red);
	draw_line(0, 0, cos(a_rot + M_PI * 0.5), sin(a_rot + M_PI * 0.5), Qt::green);
	draw_line(0, 0, cos(a_rot + M_PI * 1.0), sin(a_rot + M_PI * 1.0), Qt::cyan);
	draw_line(0, 0, cos(a_rot + M_PI * 1.5), sin(a_rot + M_PI * 1.5), Qt::magenta);

	double acc_time = 0.0;
	for(std::pair<double, double> pair: time_segments)
		acc_time += pair.second - pair.first;

	double i_samples = 30000.0 / (std::min(zoom, 4.0) / 4.0);

	for(std::pair<double, double> pair: time_segments)
		for(double t = pair.first; t <= pair.second; t += (pair.second - pair.first) / (i_samples * (pair.second - pair.first) / acc_time))
			draw_sample(t, Qt::white, Qt::gray);

	ui->preview->setPixmap(QPixmap::fromImage(out));
}

void RecordViewer::on_zoomSpinBox_valueChanged(double arg1)
{
	double zoom = ui->zoomSpinBox->value();

	int val_max = 0;
	int val_min = 0;

	if(zoom > 1.0)
	{
		val_max = (zoom * 100 - 100) / 2;
		val_min = -val_max;
	}

	if(zoom <= 3.0)
		ui->zoomSpinBox->setSingleStep(0.1);
	else
		ui->zoomSpinBox->setSingleStep(zoom / 8.0);

	QPointF pos = sliders_pos();

	ui->horizontalScrollBar->setRange(val_min, val_max);
	ui->verticalScrollBar->setRange(val_min, val_max);

	if(val_max != val_min)
	{
		ui->horizontalScrollBar->setValue((pos.x() * 0.5 + 0.5) * (double)(val_max - val_min) + (double)val_min);
		ui->verticalScrollBar->setValue((pos.y() * 0.5 + 0.5) * (double)(val_max - val_min) + (double)val_min);
	}

	refresh();
}

void RecordViewer::on_horizontalScrollBar_sliderMoved(int position)
{
	refresh();
}

void RecordViewer::on_verticalScrollBar_sliderMoved(int position)
{
	refresh();
}

void RecordViewer::on_rotationSpinBox_valueChanged(double arg1)
{
	refresh();
}

QPointF RecordViewer::sliders_pos()
{
	double zoom = ui->zoomSpinBox->value();

	int val_h = ui->horizontalScrollBar->value();
	int val_v = ui->verticalScrollBar->value();

	int val_min = ui->horizontalScrollBar->minimum();
	int val_max = ui->horizontalScrollBar->maximum();

	double pos_x = 0.0;
	double pos_y = 0.0;

	if(zoom > 1.0 && val_min != val_max)
	{
		pos_x = ((double)(val_h - val_min) / (double)(val_max - val_min)) * 2.0 - 1.0;
		pos_y = ((double)(val_v - val_min) / (double)(val_max - val_min)) * 2.0 - 1.0;
	}

	return QPointF(pos_x, pos_y);
}

void RecordViewer::resizeEvent(QResizeEvent * ev)
{
	QWidget::resizeEvent(ev);
	refresh();
}

void RecordViewer::mouseDoubleClickEvent(QMouseEvent * ev)
{
	QWidget::mouseDoubleClickEvent(ev);
}

void RecordViewer::mouseMoveEvent(QMouseEvent * ev)
{
	QWidget::mouseMoveEvent(ev);
}

void RecordViewer::mousePressEvent(QMouseEvent * ev)
{
	QWidget::mousePressEvent(ev);
}

void RecordViewer::mouseReleaseEvent(QMouseEvent * ev)
{
	QWidget::mouseReleaseEvent(ev);
}

void RecordViewer::wheelEvent(QWheelEvent * ev)
{
	QPoint numDegrees = ev->angleDelta() / 8;
	const int angleForTranslationStep = 8;
	const int angleForZoomStep = 15;
	const int angleForRotationStep = 15;

	if(ev->modifiers() & Qt::ControlModifier)
	{
		numDegrees /= angleForZoomStep;
		if(numDegrees.y())
			ui->zoomSpinBox->setValue(ui->zoomSpinBox->value() + ui->zoomSpinBox->singleStep() * (double)numDegrees.y());
	}
	else if(ev->modifiers() & Qt::ShiftModifier)
	{
		double zoom = ui->zoomSpinBox->value();

		numDegrees /= angleForRotationStep;
		if(numDegrees.y())
			ui->rotationSpinBox->setValue(ui->rotationSpinBox->value() - ui->rotationSpinBox->singleStep() * (double)numDegrees.y() / zoom);

		/*numDegrees /= angleForTranslationStep;
		if(!ui->horizontalScrollBar->isHidden() && numDegrees.y())
		{
			ui->horizontalScrollBar->setValue(
				ui->horizontalScrollBar->value() + ui->horizontalScrollBar->singleStep() * numDegrees.y());
			refresh();
		}*/
	}
	else
	{
		numDegrees /= angleForTranslationStep;
		if(!ui->verticalScrollBar->isHidden() && numDegrees.y())
		{
			ui->verticalScrollBar->setValue(
				ui->verticalScrollBar->value() - ui->verticalScrollBar->singleStep() * numDegrees.y());
			refresh();
		}

		if(!ui->horizontalScrollBar->isHidden() && numDegrees.x())
		{
			ui->horizontalScrollBar->setValue(
				ui->horizontalScrollBar->value() + ui->horizontalScrollBar->singleStep() * numDegrees.x());
			refresh();
		}
	}

	QWidget::wheelEvent(ev);
}
