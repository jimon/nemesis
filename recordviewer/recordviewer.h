#pragma once

#include <QWidget>
#include <QLabel>

#include "recorder_interface.h"

namespace Ui
{
	class RecordViewer;
}

class RecordViewerLabel : public QLabel
{
	Q_OBJECT
public:
	explicit RecordViewerLabel(QWidget * parent = 0);

signals:
	void onResize();

private:
	void resizeEvent(QResizeEvent * ev);
};

class RecordViewer : public QWidget
{
	Q_OBJECT

public:
	explicit RecordViewer(QWidget * parent = 0);
	~RecordViewer();

	void set_recorder(recorder_t * set);

public slots:
	void refresh();

private slots:
	void redraw();

	void on_zoomSpinBox_valueChanged(double arg1);
	void on_horizontalScrollBar_sliderMoved(int position);
	void on_verticalScrollBar_sliderMoved(int position);
	void on_rotationSpinBox_valueChanged(double arg1);

private:
	Ui::RecordViewer * ui;
	recorder_t * recorder;
	QTimer * refresh_timer = nullptr;

	QPointF sliders_pos();

	void resizeEvent(QResizeEvent * ev);
	void mouseDoubleClickEvent(QMouseEvent * ev);
	void mouseMoveEvent(QMouseEvent * ev);
	void mousePressEvent(QMouseEvent * ev);
	void mouseReleaseEvent(QMouseEvent * ev);
	void wheelEvent(QWheelEvent * ev);
};
