# Vinyl Nemesis

## Intro

Vinyl Nemesis is an open source simulation toolkit of all aspects of vinyl disc mastering and playing.

## Packages

* Simulation
* Record Viewer
* Editor

## Pkg: Simulation

Stand alone C++ library which contains everything required for simulation of all aspects of vinyl record

## Pkg: Record Viewer

Show you actual record in real-time

## Pkg: Editor

Editor window, contains other widgets
 