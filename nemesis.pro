# Nemesis

CONFIG += c++11

INCLUDEPATH += $$PWD

INCLUDEPATH += simulation
include(simulation/simulation.pri)

INCLUDEPATH += recordviewer
include(recordviewer/recordviewer.pri)

INCLUDEPATH += oscilloscope
include(oscilloscope/oscilloscope.pri)

INCLUDEPATH += editor
include(editor/editor.pri)

win32 {
	RC_FILE = editor/editor.rc
}

VPATH += $$INCLUDEPATH
