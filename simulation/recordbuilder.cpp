#include "recordbuilder.h"
#include <QTimer>
#include <math.h>

//#define DECODE_FILE

recordbuilder::recordbuilder(QObject *parent) :
	QObject(parent)
	//m_decoder(new QAudioDecoder(this))
{
	//connect(m_decoder, SIGNAL(bufferReady()), this, SLOT(bufferReady()));
	//connect(m_decoder, SIGNAL(finished()), this, SLOT(finished()));
	//connect(m_decoder, SIGNAL(error(QAudioDecoder::Error)), this, SLOT(error()));

	m_data.reserve(16 * 1024 * 1024);
}

void recordbuilder::process(QString filename)
{
	m_data.clear();

	/*QAudioFormat format;
	format.setChannelCount(2);
	format.setSampleSize(16);
	format.setSampleRate(44100);
	format.setCodec("audio/pcm");
	format.setSampleType(QAudioFormat::SignedInt);
	m_decoder->setAudioFormat(format);*/

#ifdef DECODE_FILE
	m_decoder->setSourceFilename(filename);
	m_decoder->start();
#else
	double sample_rate = 144100.0;
	double freq1 = 250.0;
	double freq2 = 250.0;
	double freq3 = 50.0;
	double freq4 = 40.0;
	for(double t = 0.0; t < 10.0; t += 1.0 / sample_rate)
	{
		double v1 = sin(t * freq1 * 3.14159265358979323846 * 2.0);
		double v2 = sin(t * freq2 * 3.14159265358979323846 * 2.0);
		double v3 = sin(t * freq3 * 3.14159265358979323846 * 2.0);
		double v4 = sin(t * freq4 * 3.14159265358979323846 * 2.0);

		//v1 *= v3;
		//v2 *= v4;

		int16_t sample1 = (int16_t)(v1 * (double)INT16_MAX);
		int16_t sample2 = (int16_t)(v2 * (double)INT16_MAX);

		m_data.push_back(sample1);
		m_data.push_back(sample2);
	}
	QTimer::singleShot(10, this, SLOT(finished()));
#endif
}

record *recordbuilder::get_record()
{
	record *rec = new record;
	rec->setup(m_data);
	return rec;
}

void recordbuilder::bufferReady()
{
	static bool test = false;

	if(test)
		return;

	//QAudioBuffer data = m_decoder->read();

	//int16_t *temp = (int16_t*)data.constData();
	//for(int i = 0; i < data.sampleCount(); ++i)
	//	m_data.push_back(temp[i]);

	//if(m_data.size() > 1024 * 1024)
	//{
	//	test = true;
	//	m_decoder->stop();
	//	finished();
	//}
}

void recordbuilder::finished()
{
	//qDebug() << "finished decoding, samples" << m_data.size();
	emit ready();
}

void recordbuilder::error()
{
	//qWarning() << "decoding error" << m_decoder->errorString();
	emit ready();
}
