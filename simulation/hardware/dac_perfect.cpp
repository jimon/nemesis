#include "dac_perfect.h"
#include <math.h>

signal_sample_t dac_perfect_t::sample(double t)
{
	if(!library)
		return signal_sample_t();

	double point = t * (double)library->sample_rate();
	double param = point - floor(point);

	size_t prev_sample = (size_t)floor(point);
	size_t next_sample = (size_t)ceil(point);

	double al = library->sample_left(prev_sample);
	double ar = library->sample_right(prev_sample);
	double bl = library->sample_left(next_sample);
	double br = library->sample_right(next_sample);

	signal_sample_t out;
	out.l = (bl - al) * param + al;
	out.r = (br - ar) * param + ar;

	return line_out.to_line(out);
}
