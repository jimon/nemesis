#pragma once

/*
general line level config

more info about line level : http://en.wikipedia.org/wiki/Line_level
more info about dBV <-> dBu <-> voltage : http://www.sengpielaudio.com/calculator-db-volt.htm

reference signal - signal with amplitude [-1, +1] volts
*/

#include "signal_sample.h"

const double line_v_peak_consumer = 0.447;	// consumer audio, -10 dBV
const double line_v_peak_pro = 1.736;		// pro, 1.781512501 dBV

struct line_t
{
	double v_peak = line_v_peak_consumer;

	signal_sample_t to_line(signal_sample_t reference_signal); // convert reference signal [-1, +1] volts to line
	signal_sample_t from_line(signal_sample_t line_signal); // convert line signal to reference signal
};
