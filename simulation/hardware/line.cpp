#include "line.h"

signal_sample_t line_t::to_line(signal_sample_t reference_signal)
{
	return reference_signal * v_peak;
}

signal_sample_t line_t::from_line(signal_sample_t line_signal)
{
	return line_signal / v_peak;
}
