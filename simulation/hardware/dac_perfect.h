#pragma once

/*
The Perfect Dac
*/

#include "dac_interface.h"

class dac_perfect_t : public dac_t
{
public:
	signal_sample_t sample(double t);
};
