#pragma once

#include "equalizer_interface.h"

class equalizer_perfect_t : public equalizer_t
{
public:
	signal_sample_t sample(double t);
};
