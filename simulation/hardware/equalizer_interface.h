#pragma once

#include "signal_source.h"

class equalizer_t : public signal_filter_t
{
public:
	enum mode_t
	{
		MOVE_PASS_THROUGH,
		MODE_RIAA_PLAYBACK,
		MODE_RIAA_RECORDING
	};

	virtual void	set_mode(mode_t set) {mode = set;}
	virtual mode_t	get_mode() const {return mode;}

protected:
	mode_t mode = MOVE_PASS_THROUGH;
};
