#pragma once

struct signal_sample_t
{
	double l = 0.0;
	double r = 0.0;

	signal_sample_t() {}
	signal_sample_t(double set_v):l(set_v), r(set_v){}
	signal_sample_t(double set_l, double set_r):l(set_l), r(set_r){}

	signal_sample_t &	operator +=(double k) {l += k; r += k; return *this;}
	signal_sample_t &	operator -=(double k) {l -= k; r -= k; return *this;}
	signal_sample_t &	operator *=(double k) {l *= k; r *= k; return *this;}
	signal_sample_t &	operator /=(double k) {l /= k; r /= k; return *this;}

	signal_sample_t		operator +(double k) {return signal_sample_t(l + k, r + k);}
	signal_sample_t		operator -(double k) {return signal_sample_t(l - k, r - k);}
	signal_sample_t		operator *(double k) {return signal_sample_t(l * k, r * k);}
	signal_sample_t		operator /(double k) {return signal_sample_t(l / k, r / k);}

	signal_sample_t		operator +() {return signal_sample_t(+l, +r);}
	signal_sample_t		operator -() {return signal_sample_t(-l, -r);}
};
