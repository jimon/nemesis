#include "recorder_perfect.h"

#define _USE_MATH_DEFINES
#include <math.h>

groove_sample_t recorder_perfect_t::sample(double t)
{
	if(!source)
		return groove_sample_t();

	signal_sample_t sample = line_in.from_line(source->sample(t));

	groove_sample_t out;
	out.a = spiral.get_angular_position(t);
	out.r = spiral.get_radius(t);

	#define CUT_ANGLE 90
	#define GROOVE_HEIGHT 200 // micro meters down in record for 1 volts
	#define GROOVE_HEIGHT_OFFSET 50 // micro meters down in record for 0 volts

	/*
	____x     y____
		\     /
		 \   /
		  \ /
		   z
	*/

	const double angle = sqrt(2.0) / 2.0;
	const double angle2 = tan(CUT_ANGLE * M_PI / 360.0);
	const double ratio = GROOVE_HEIGHT / sqrt(2.0);

	double p_x = -sample.l * angle - sample.r * angle;
	double p_y =  sample.l * angle - sample.r * angle;

	double z = p_x * ratio;
	double z_h = p_y * ratio + GROOVE_HEIGHT + GROOVE_HEIGHT_OFFSET;

	if(z_h < 0.0)
		z_h = 0.0; // mega stereo effects anyone ?

	out.r += z;
	out.w = z_h * angle2 * 2.0;
	out.d = z_h;

	return out;
}
