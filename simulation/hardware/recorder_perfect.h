#pragma once

#include "recorder_interface.h"

class recorder_perfect_t : public recorder_t
{
public:
	groove_sample_t sample(double t);
};
