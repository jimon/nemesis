#pragma once

#include "signal_source.h"
#include "groove.h"
#include "groove_spiral.h"

class recorder_t : public signal_capture_t
{
public:
	virtual void					set_spiral(const groove_spiral_t & set) {spiral = set;}
	virtual groove_spiral_t &		get_spiral() {return spiral;}
	virtual const groove_spiral_t &	get_spiral() const {return spiral;}

	virtual groove_sample_t			sample(double t) = 0;

protected:
	groove_spiral_t spiral;
};

