
#include "equalizer_perfect.h"

#include <cstddef>

#define _USE_MATH_DEFINES
#include <math.h>

#define CORES_441KHZ
//#define CORES_96KHZ

double equalizer_riaa_playback(double v[], size_t n)
{
#ifdef CORES_96KHZ
	double a[3] = {1.0000000000, -0.8535331000, -0.1104595113};
	double b[3] = {1.0000000000, -1.8666083000,  0.8670382873};
#endif

#ifdef CORES_441KHZ
	double a[3] = {1.00000000000000, -0.73845850035973, -0.17951755477430};
	double b[3] = {0.02675918611906, -0.04592084787595,  0.01921229297239};
#endif

	double in[3] = {0.0, 0.0, 0.0};
	double out[3] = {0.0, 0.0, 0.0};
	for(size_t i = 0; i < n; ++i)
	{
		in[2] = in[1];
		in[1] = in[0];
		in[0] = v[i];
		out[2] = out[1];
		out[1] = out[0];
		out[0] = b[0] * in[0] + b[1] * in[1] + b[2] * in[2] - a[1] * out[1] - a[2] * out[2];
	}

	return out[0];
}

double equalizer_riaa_recording(double v[], size_t n)
{
#ifdef CORES_96KHZ
	double a[3] = {1.0000000000, -1.8666083000,  0.8670382873};
	double b[3] = {1.0000000000, -0.8535331000, -0.1104595113};
#endif

#ifdef CORES_441KHZ
	double a[3] = {0.02675918611906, -0.04592084787595,  0.01921229297239};
	double b[3] = {1.00000000000000, -0.73845850035973, -0.17951755477430};
#endif

	b[2] /= a[0];
	b[1] /= a[0];
	b[0] /= a[0];
	a[2] /= a[0];
	a[1] /= a[0];
	a[0] /= a[0];

	double in[3] = {0.0, 0.0, 0.0};
	double out[3] = {0.0, 0.0, 0.0};
	for(size_t i = 0; i < n; ++i)
	{
		in[2] = in[1];
		in[1] = in[0];
		in[0] = v[i];
		out[2] = out[1];
		out[1] = out[0];
		out[0] = b[0] * in[0] + b[1] * in[1] + b[2] * in[2] - a[1] * out[1] - a[2] * out[2];
	}

	return out[0];
}

signal_sample_t equalizer_perfect_t::sample(double t)
{
	if(!source)
		return signal_sample_t();

	/*
	No time for perfect filter yet, so let's play a game

	http://www.musicdsp.org/showone.php?id=122

	96kHz
	1)
	b = [ 0.05265477122714  -0.09864197097385   0.04596474352090  ]
	a = [  1.00000000000000  -0.85835597216218  -0.10600020417219 ]

	2)
	a = [ 1.0000000000 -1.8666083000 0.8670382873 ]
	b = [ 1.0000000000 -0.8535331000 -0.1104595113 ]
	error +/- 0.006dB

	out = b0*in[0] + b1*in[-1] + b2*in[-2] - a1*out[-1] - a2*out[-2]
	*/

#ifdef CORES_441KHZ
	const double step = 1.0 / 44100.0;
#endif
#ifdef CORES_96KHZ
	const double step = 1.0 / 96000.0;
#endif
	const size_t n = 512;
	const double td = (double)n * step;
	double vl[n], vr[n];

	size_t nt = n - 1;
	for(double t2 = t; t2 > t - td; t2 -= step)
	{
		signal_sample_t in = line_in.from_line(source->sample(t2));
		vl[nt] = in.l;
		vr[nt] = in.r;
		if(nt == 0)
			break;
		else
			nt--;
	}

	signal_sample_t out;

	switch(mode)
	{
	case MOVE_PASS_THROUGH:
		out.l = vl[n - 1];
		out.r = vr[n - 1];
		break;
	case MODE_RIAA_PLAYBACK:
		out.l = equalizer_riaa_playback(vl, n);
		out.r = equalizer_riaa_playback(vr, n);
		break;
	case MODE_RIAA_RECORDING:
		out.l = equalizer_riaa_recording(vl, n);
		out.r = equalizer_riaa_recording(vr, n);
		break;
	default:
		break;
	}

	if(out.l < -1.0) out.l = -1.0;
	if(out.l >  1.0) out.l =  1.0;
	if(out.r < -1.0) out.r = -1.0;
	if(out.r >  1.0) out.r =  1.0;

	return line_out.to_line(out);
}
