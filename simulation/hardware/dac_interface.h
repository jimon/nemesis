#pragma once

/*
general DAC interface
*/

#include "signal_source.h"
#include "library.h"

class dac_t : public signal_source_t
{
public:
	virtual ~dac_t() {}

	virtual void		set_library(library_t * set) {library = set;}
	virtual library_t *	get_library() const {return library;}

protected:
	library_t * library = nullptr;
};
