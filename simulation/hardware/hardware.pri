CONFIG += c++11

SOURCES += line.cpp
HEADERS += line.h

HEADERS += signal_sample.h
HEADERS += signal_source.h

HEADERS += dac_interface.h
SOURCES += dac_perfect.cpp
HEADERS += dac_perfect.h

HEADERS += player_interface.h
SOURCES += player_perfect.cpp
HEADERS += player_perfect.h

HEADERS += recorder_interface.h
SOURCES += recorder_perfect.cpp
HEADERS += recorder_perfect.h

HEADERS += equalizer_interface.h
SOURCES += equalizer_perfect.cpp
HEADERS += equalizer_perfect.h
