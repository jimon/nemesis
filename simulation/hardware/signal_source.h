#pragma once

#include "line.h"

class signal_source_t
{
public:
	virtual ~signal_source_t() {}

	virtual void				set_line_out(const line_t & set) {line_out = set;}
	virtual line_t &			get_line_out() {return line_out;}
	virtual const line_t &		get_line_out() const {return line_out;}

	virtual signal_sample_t		sample(double t) = 0;

protected:
	line_t line_out;
};

class signal_capture_t
{
public:
	virtual ~signal_capture_t() {}

	virtual void				set_source(signal_source_t * set) {source = set;}
	virtual signal_source_t *	get_source() const {return source;}

	virtual void				set_line_in(const line_t & set) {line_in = set;}
	virtual line_t &			get_line_in() {return line_in;}
	virtual const line_t &		get_line_in() const {return line_in;}

protected:
	signal_source_t * source = nullptr;
	line_t line_in;
};

class signal_filter_t : public signal_source_t, public signal_capture_t
{
public:
	virtual ~signal_filter_t() {}
};

