#pragma once

#include <vector>

/*
sound source library
*/

class library_t
{
public:

	// TODO
	// interface to load-save

	double total_length() {return (double)total_samples() / (double)sample_rate();}

	//void read_ahead(size_t from, size_t to); // try to cache

	size_t sample_rate();
	size_t total_samples();

	double sample_left(size_t sample);  // return sample in range [-1, 1]
	double sample_right(size_t sample); // return sample in range [-1, 1]

protected:

};
