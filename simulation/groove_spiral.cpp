#include "groove_spiral.h"
#include <algorithm>

#define _USE_MATH_DEFINES
#include <math.h>

double groove_spiral_t::get_angular_position(double t) const
{
	double minutes = t / 60.0;
	double rotations = minutes * speed;

	return rotations * 2.0 * M_PI;
}

double groove_spiral_t::get_radius(double time) const
{
	double max_radius = get_radius_max();

	for(size_t i = 0; i < data.size(); ++i)
		if(data[i].first >= time)
		{
			if(i)
			{
				auto a = data[i - 1];
				auto b = data[i];
				double t = (time - a.first) / (b.first - a.first);
				return std::min((b.second - a.second) * t + a.second, max_radius);
			}
			else
				return std::min(data[i].second, max_radius);
		}
	return std::min(data.back().second, max_radius);
}

double groove_spiral_t::get_radius_max() const
{
	return diameter * 0.0254 * 1000000.0 / 2.0;
}
