
SOURCES += project.cpp
HEADERS += project.h

SOURCES += groove.cpp
HEADERS += groove.h

SOURCES += groove_spiral.cpp
HEADERS += groove_spiral.h

SOURCES += library.cpp
HEADERS += library.h

INCLUDEPATH += $$PWD/hardware
include(hardware/hardware.pri)

SOURCES += record.cpp
HEADERS += record.h

SOURCES += recordbuilder.cpp
HEADERS += recordbuilder.h

