#pragma once

#include <QObject>
#include <QVector>
#include <QPair>
#include <stdint.h>

/*
basic groove information
http://www.vinylrecorder.com/stereo.html
http://londonjazzcollector.files.wordpress.com/2012/08/vonylgrooveelctronmicro.jpg

our modulation is same with 90 deg stereo groove and triangle shaped cut head
____x     y____
    \     /
     \   /
      \ /
       z

we are defining base cut angle (xz to xz) for whole track, 90 by default
each sample contais position of x and y relative to 0.0 (zero signal)
position is defined in micro meters

normal distance from x to y should be around 50 micro meters

maximum amlitude can be around 1 mm
*/

#define SAMPLE_RATE 144100
#define CUT_ANGLE 90
#define GROOVE_HEIGHT 20 // micro meters down in record for 1 volts
#define GROOVE_HEIGHT_OFFSET 50 // micro meters down in record for 0 volts

#define GROOVE_MAX_OFFSET ((GROOVE_HEIGHT + GROOVE_HEIGHT_OFFSET) * 2.0 / 1.41421356237 + (GROOVE_HEIGHT + GROOVE_HEIGHT_OFFSET))

struct recordsample_t
{
	double t = 0.0;
	double x = 0.0, y = 0.0;
	double z = 0.0f, z_h = 0.0;

	recordsample_t &from(double time, double left, double right);
	QPair<double, double> to();
};

class record : public QObject
{
	Q_OBJECT
public:
	explicit record(QObject *parent = 0);

	void setup(const QVector<int16_t> &data);
	double length();
	recordsample_t sample(double t);
signals:

public slots:

protected:
	QVector<int16_t> m_data;
	QPair<double, double> data_sample(double t);
};
