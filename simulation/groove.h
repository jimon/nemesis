#pragma once

/*
groove sample from record

-------------->r
____land____<--w-->____
			\     /
			 \   / wall
			  \ /
			   d
groove sample structure contains :
a - angle from start of record, in radians
r - radius from center of record, in micrometers
w - width of top of groove, in micrometers
d - depth of groove, in micrometers
*/

struct groove_sample_t
{
	double a = 0.0;
	double r = 0.0;
	double w = 0.0;
	double d = 0.0;
};
