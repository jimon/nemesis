#include "record.h"
#include <QDebug>
#include <math.h>

recordsample_t &recordsample_t::from(double time, double left, double right)
{
	t = time;

	/*
	____x     y____
	    \     /
	     \   /
	      \ /
	       z
	*/

	const double angle = sqrt(2.0) / 2.0;
	const double angle2 = tan(CUT_ANGLE * 3.14159265358979323846 / 360.0);
	const double ratio = GROOVE_HEIGHT / sqrt(2.0);

	//qDebug() << left << right;

	double p_x = 0.0;
	double p_y = 0.0;

	p_x -= left * angle;
	p_y += left * angle;
	p_x -= right * angle;
	p_y -= right * angle;

	//qDebug() << p_x << p_y;

	z = p_x * ratio;
	z_h = p_y * ratio + GROOVE_HEIGHT + GROOVE_HEIGHT_OFFSET;

	if(z_h < 0.0)
		z_h = 0.0; // mega stereo effects anyone ?

	//qDebug() << z << z_h;

	x = z - z_h * angle2;
	y = z + z_h * angle2;

	//qDebug() << x << y << z_h;

	return *this;
}

QPair<double, double> recordsample_t::to()
{
	return {0, 0};
}

record::record(QObject *parent) :
	QObject(parent)
{
}

void record::setup(const QVector<int16_t> &data)
{
	m_data = data;
}

double record::length()
{
	return ((double)m_data.size()) / 2.0 / (double)SAMPLE_RATE;
}

recordsample_t record::sample(double t)
{
	QPair<double, double> s = data_sample(t);
	return recordsample_t().from(t, s.first, s.second);
}

QPair<double, double> record::data_sample(double t)
{
	// simulate linear disk cutter
	double point = t * (double)SAMPLE_RATE;

	int i = (int)floor(point);
	int j = (int)ceil(point);

	int il = m_data.value(i * 2 + 0, 0);
	int ir = m_data.value(i * 2 + 1, 0);
	int jl = m_data.value(j * 2 + 0, 0);
	int jr = m_data.value(j * 2 + 1, 0);

	double al = ((double)il) / (double)INT16_MAX;
	double ar = ((double)ir) / (double)INT16_MAX;
	double bl = ((double)jl) / (double)INT16_MAX;
	double br = ((double)jr) / (double)INT16_MAX;

	//qDebug() << i << j << il << ir << jl << jr;

	double p = point - floor(point);

	return {(bl - al) * p + al, (br - ar) * p + ar};
}
