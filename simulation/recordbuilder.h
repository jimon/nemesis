#pragma once

#include <QObject>
//#include <QAudioDecoder>
#include <QVector>
#include <stdint.h>
#include "record.h"

class recordbuilder : public QObject
{
	Q_OBJECT
public:
	explicit recordbuilder(QObject * parent = 0);

	void process(QString filename);
	bool in_progress() {return false;}

	record * get_record();

signals:
	void ready();

public slots:

protected slots:
	void bufferReady();
	void finished();
	void error();

protected:
	//QAudioDecoder *m_decoder;
	QVector<int16_t> m_data; // layout of sample is [left channel, right channel]
};
