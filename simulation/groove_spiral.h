#pragma once

#include <vector>

/*
spiral information of groove
data is encoded in format [time >= 0 (secs), radius_from_center >= 0 (micrometers)]
speed is in RPM
diameter is in inches
playtime - total length of record, in seconds
*/

struct groove_spiral_t
{
	std::vector<std::pair<double, double>> data;
	double speed = 33.3;
	double diameter = 12.0;
	double playtime = 60.0;

	double get_angular_position(double t) const; // return angle in radians of position on disc for time (secs)
	double get_radius(double t) const; // return radius for current time (secs)
	double get_radius_max() const; // return maximal radius in micrometers
};
