#include "library.h"

#define LIBRARY_TEST

#ifdef LIBRARY_TEST

#define SAMPLE_RATE 192000
#define TOTAL_LENGTH (60 * SAMPLE_RATE)

size_t library_t::sample_rate()
{
	return SAMPLE_RATE;
}

size_t library_t::total_samples()
{
	return TOTAL_LENGTH;
}

#define _USE_MATH_DEFINES
#include <math.h>

double simple_sin_source(double t, double freq = 400.0, double phase = 0.0)
{
	if(t < 6.0)
		return 0.0;
	if(t > 50.0)
		return 0.0;
	return sin(t * freq * M_PI * 2.0 + phase);
}

double library_t::sample_left(size_t sample)
{
	double t = (double)sample / (double)sample_rate();
	return simple_sin_source(t, 20.0) + simple_sin_source(t, 4000.0) * 0.1;
}

double library_t::sample_right(size_t sample)
{
	double t = (double)sample / (double)sample_rate();
	return simple_sin_source(t) * 0.2;
}

#else

#endif
