#pragma once

#include <QString>

/*
basic groove information
http://www.vinylrecorder.com/stereo.html
http://londonjazzcollector.files.wordpress.com/2012/08/vonylgrooveelctronmicro.jpg
our modulation is same with 90 deg stereo groove and triangle shaped cut head
*/

class project_t
{
public:
	void load(QString root_file);
	void save();

protected:
};
